"""
 █████  ██    ██ ████████  ██████      ██████  ██       ██████   ██████   ██████  ███████ ██████  
██   ██ ██    ██    ██    ██    ██     ██   ██ ██      ██    ██ ██       ██       ██      ██   ██ 
███████ ██    ██    ██    ██    ██     ██████  ██      ██    ██ ██   ███ ██   ███ █████   ██████  
██   ██ ██    ██    ██    ██    ██     ██   ██ ██      ██    ██ ██    ██ ██    ██ ██      ██   ██ 
██   ██  ██████     ██     ██████      ██████  ███████  ██████   ██████   ██████  ███████ ██   ██ 
                                                                                                  
                                                                                                  

"""



import tweepy

# Add your Twitter API credentials here
consumer_key = "your_consumer_key"
consumer_secret = "your_consumer_secret"
access_token = "your_access_token"
access_token_secret = "your_access_token_secret"

# Authenticate to Twitter
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# Create API object
api = tweepy.API(auth)

try:
    # Send a tweet
    api.update_status("This is a tweet sent using the Twitter API and Python.")
    print("Tweet sent successfully!")
except tweepy.TweepError as error:
    # If there is an error, print the error message
    print("Error sending tweet: " + str(error))
