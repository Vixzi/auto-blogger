from typing import Tuple
import mysql.connector
from chatGPT_api import generate
from datetime import datetime



"""
This class right here is to initialize the database by connecting 
into the MySQL database 
"""

class CONNECT_DATABASE():
        def connectDataBase() -> Tuple[mysql.connector.connection.MySQLConnection, mysql.connector.cursor.MySQLCursor]:
                connection_config = mysql.connector.connect(
                       host="localhost",
                       user="root",
                       password="admin",
                       database="sys" 
                )
                connection_cursor = connection_config.connection_cursor()
                return connection_config, connection_cursor

        connectDataBase()

CONNECT_DATABASE()