from typing import Tuple
import mysql.connector
from chatGPT_api import generate
from datetime import datetime

def connect_to_db() -> Tuple[mysql.connector.connection.MySQLConnection, mysql.connector.cursor.MySQLCursor]:
    # Connect to MySQL database and return connection and cursor objects
    conn = mysql.connector.connect(
        host="localhost",
        user="root",
        password="admin",
        database="sys"
    )
    cursor = conn.cursor()
    return conn, cursor

def insert_into_prompt_table(cursor: mysql.connector.cursor.MySQLCursor, prompt_id: int, prompt_text: str) -> None:
    # Insert a new prompt into the 'prompt' table
    insert_query = "INSERT INTO prompt (prompt) VALUES (%s)"
    values = (prompt_text,)
    cursor.execute(insert_query, values)

def insert_into_post_table(cursor: mysql.connector.cursor.MySQLCursor, post_id: int, post_text: str) -> None:
    # Insert a new post into the 'post' table
    insert_query = "INSERT INTO post (post_text, post_date, post_time) VALUES (%(post_text)s, %(post_date)s, %(post_time)s)"
    now = datetime.now()
    # Format the date as YYYY-MM-DD
    date_str = now.strftime('%Y-%m-%d')
    # Format the time as HH:MM:SS
    time_str = now.strftime('%H:%M:%S')
    values = {#'post_id': int(post_id), 
              'post_text': post_text, 'post_date': date_str, 'post_time': time_str}
    cursor.execute(insert_query, values)


def generate_posts_for_prompt(prompt: str = "") -> zip[int, zip[str,str]]:
    # Generate a dictionary of posts for a given prompt
    if len(prompt) != 0:
        posts = generate(prompt)
    else:
        posts = generate()
    return posts


def main():
    # Connect to the database
    conn, cursor = connect_to_db()

    #Prompt the user for a subject area and generate posts
    root_prompt = input("Enter a single line subject area to generate posts for, or press enter to continue.\n")
    posts = generate_posts_for_prompt(root_prompt)

    # Insert the posts into the database
    i= 1000
    for i, pair in enumerate(posts):
        prompt_id = i
        prompt_text = pair[1][0]
        post_id = i
        post_text = pair[1][1]
        insert_into_prompt_table(cursor, prompt_id, prompt_text)
        insert_into_post_table(cursor, post_id, post_text)

    # Commit the changes and close the connection
    conn.commit()
    cursor.close()
    conn.close()

if __name__ == '__main__':
    main()